#include <cctk.h>
#include <cctk_Parameters.h>
#include <cctk_Arguments.h>

#include <algorithm>
#include <cassert>
#include <future>
#include <vector>
using namespace std;

namespace {
vector<future<void> > fs_interior;
vector<future<void> > fs_sync;
}

extern "C" void WaveToyFunHPC_Cxx11_Evolution(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;

  // Set up shorthands

  CCTK_REAL dx = CCTK_DELTA_SPACE(0);
  CCTK_REAL dy = CCTK_DELTA_SPACE(1);
  CCTK_REAL dz = CCTK_DELTA_SPACE(2);
  CCTK_REAL dt = CCTK_DELTA_TIME;

  CCTK_REAL dx2 = dx * dx;
  CCTK_REAL dy2 = dy * dy;
  CCTK_REAL dz2 = dz * dz;
  CCTK_REAL dt2 = dt * dt;

  CCTK_REAL dt2_dx2i = dt2 / dx2;
  CCTK_REAL dt2_dy2i = dt2 / dy2;
  CCTK_REAL dt2_dz2i = dt2 / dz2;

  CCTK_REAL factor = 2 - 2 * (dt2_dx2i + dt2_dy2i + dt2_dz2i);

  int istart = cctk_nghostzones[0];
  int jstart = cctk_nghostzones[1];
  int kstart = cctk_nghostzones[2];

  int iend = cctk_lsh[0] - cctk_nghostzones[0];
  int jend = cctk_lsh[1] - cctk_nghostzones[1];
  int kend = cctk_lsh[2] - cctk_nghostzones[2];

  auto val = [cctkGH](auto gridfunc, auto i, auto j, auto k) {
    return gridfunc[CCTK_GFINDEX3D(cctkGH, i, j, k)];
  };

  // (16+2*1)^3 * 3*8 B = 140 kB < 256 kB (L2 cache)
  int iblock = 16;
  int jblock = 16;
  int kblock = 16;

  assert(iblock >= cctk_nghostzones[0]);
  assert(jblock >= cctk_nghostzones[1]);
  assert(kblock >= cctk_nghostzones[2]);

  // Do the evolution

  for (int k0 = kstart; k0 < kend; k0 += kblock) {
    for (int j0 = jstart; j0 < jend; j0 += jblock) {
      for (int i0 = istart; i0 < iend; i0 += iblock) {

        int i1 = min(iend, i0 + iblock);
        int j1 = min(jend, j0 + jblock);
        int k1 = min(kend, k0 + kblock);

        bool is_sync_boundary =
            (cctk_bbox[0] && i0 == istart) || (cctk_bbox[1] && i1 == iend) ||
            (cctk_bbox[2] && j0 == jstart) || (cctk_bbox[3] && j1 == jend) ||
            (cctk_bbox[4] && k0 == kstart) || (cctk_bbox[5] && k1 == kend);
        auto &fs = is_sync_boundary ? fs_sync : fs_interior;

        fs.push_back(async(launch::async, [=]() {
          for (int k = k0; k < k1; k++) {
            for (int j = j0; j < j1; j++) {
              for (int i = i0; i < i1; i++) {
                int vindex = CCTK_GFINDEX3D(cctkGH, i, j, k);

                phi[vindex] =
                    factor * val(phi_p, i, j, k) - val(phi_p_p, i, j, k) +
                    (dt2_dx2i *
                         (val(phi_p, i + 1, j, k) + val(phi_p, i - 1, j, k)) +
                     dt2_dy2i *
                         (val(phi_p, i, j + 1, k) + val(phi_p, i, j - 1, k)) +
                     dt2_dz2i *
                         (val(phi_p, i, j, k + 1) + val(phi_p, i, j, k - 1)));
              }
            }
          }
        }));
      }
    }
  }
}

extern "C" void WaveToyFunHPC_Cxx11_Boundaries(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  fs_interior.push_back(async(launch::async, [=]() {
    int ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                                       "wavetoy::phi", bound);
    if (ierr)
      CCTK_ERROR(
          "WaveToyFunHPC_Cxx11_Boundaries: Error selecting boundary condition");
  }));

  for (auto &f : fs_sync)
    f.wait();
  fs_sync.clear();
}

extern "C" void WaveToyFunHPC_Cxx11_Finish(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;

  for (auto &f : fs_interior)
    f.wait();
  fs_interior.clear();
}
