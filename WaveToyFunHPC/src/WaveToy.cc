#include <cctk.h>
#include <cctk_Parameters.h>
#include <cctk_Arguments.h>

extern "C" void WaveToyFunHPC_Evolution(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;

  // Set up shorthands

  CCTK_REAL dx = CCTK_DELTA_SPACE(0);
  CCTK_REAL dy = CCTK_DELTA_SPACE(1);
  CCTK_REAL dz = CCTK_DELTA_SPACE(2);
  CCTK_REAL dt = CCTK_DELTA_TIME;

  CCTK_REAL dx2 = dx * dx;
  CCTK_REAL dy2 = dy * dy;
  CCTK_REAL dz2 = dz * dz;
  CCTK_REAL dt2 = dt * dt;

  CCTK_REAL dt2_dx2i = dt2 / dx2;
  CCTK_REAL dt2_dy2i = dt2 / dy2;
  CCTK_REAL dt2_dz2i = dt2 / dz2;

  CCTK_REAL factor = 2 - 2 * (dt2_dx2i + dt2_dy2i + dt2_dz2i);

  int istart = cctk_nghostzones[0];
  int jstart = cctk_nghostzones[1];
  int kstart = cctk_nghostzones[2];

  int iend = cctk_lsh[0] - cctk_nghostzones[0];
  int jend = cctk_lsh[1] - cctk_nghostzones[1];
  int kend = cctk_lsh[2] - cctk_nghostzones[2];

  auto val = [cctkGH](auto gridfunc, auto i, auto j, auto k) {
    return gridfunc[CCTK_GFINDEX3D(cctkGH, i, j, k)];
  };

  // Do the evolution

  for (int k = kstart; k < kend; k++) {
    for (int j = jstart; j < jend; j++) {
      for (int i = istart; i < iend; i++) {
        int vindex = CCTK_GFINDEX3D(cctkGH, i, j, k);

        phi[vindex] =
            factor * val(phi_p, i, j, k) - val(phi_p_p, i, j, k) +
            (dt2_dx2i * (val(phi_p, i + 1, j, k) + val(phi_p, i - 1, j, k)) +
             dt2_dy2i * (val(phi_p, i, j + 1, k) + val(phi_p, i, j - 1, k)) +
             dt2_dz2i * (val(phi_p, i, j, k + 1) + val(phi_p, i, j, k - 1))) +
            1.0e-12 * sin(sin(sin(sin(
                          sin(sin(sin(sin(sin(sin(val(phi_p, i, j, k)))))))))));
      }
    }
  }
}

extern "C" void WaveToyFunHPC_Boundaries(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  int ierr = Boundary_SelectVarForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                                     "wavetoy::phi", bound);
  if (ierr)
    CCTK_ERROR("WaveToyFunHPC_Boundaries: Error selecting boundary condition");
}
