#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include <LoopControlQ.hpp>
using LoopControlQ::iarray;

#include <qthread/future.hpp>
using namespace qthread;

#include <algorithm>
#include <array>
#include <cassert>
#include <cstdlib>
#include <iterator>
#include <memory>
#include <utility>
#include <vector>
using namespace std;

namespace {
vector<future<void> > fs_rhs;

unique_ptr<promise<void> > ps_sync;
unique_ptr<future<void> > fs_sync;
}

extern "C" void WaveToyMoLFunHPC_FunHPC_Start(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;

  assert(fs_rhs.empty());

  assert(!ps_sync);
  assert(!fs_sync);
  ps_sync = make_unique<promise<void> >();
  fs_sync = make_unique<future<void> >(ps_sync->get_future());
}

extern "C" void WaveToyMoLFunHPC_FunHPC_RHS(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;

  if (*skip_rhs)
    return;

  // Set up shorthands

  CCTK_REAL dx = CCTK_DELTA_SPACE(0);
  CCTK_REAL dy = CCTK_DELTA_SPACE(1);
  CCTK_REAL dz = CCTK_DELTA_SPACE(2);

  CCTK_REAL dx2 = dx * dx;
  CCTK_REAL dy2 = dy * dy;
  CCTK_REAL dz2 = dz * dz;

  CCTK_REAL dx2i = 1.0 / dx2;
  CCTK_REAL dy2i = 1.0 / dy2;
  CCTK_REAL dz2i = 1.0 / dz2;

  array<array<bool, 3>, 2> ibbox;
  for (int f = 0; f < 2; ++f)
    for (int d = 0; d < 3; ++d)
      ibbox[f][d] = cctk_bbox[2 * d + f];

  iarray<3> imin{cctk_nghostzones[0], cctk_nghostzones[1], cctk_nghostzones[2]};
  iarray<3> imax{cctk_lsh[0] - cctk_nghostzones[0],
                 cctk_lsh[1] - cctk_nghostzones[1],
                 cctk_lsh[2] - cctk_nghostzones[2]};

  constexpr ptrdiff_t di = 1;
  ptrdiff_t dj = di * cctk_ash[0];
  ptrdiff_t dk = dj * cctk_ash[1];

  // (16+2*1)^3 * 3*8 B = 140 kB < 256 kB (L2 cache)
  iarray<3> iblock{16, 16, 16};

  for (int d = 0; d < 3; ++d)
    assert(iblock[d] >= cctk_nghostzones[d]);

  iarray<3> iash{cctk_ash[0], cctk_ash[1], cctk_ash[2]};

  // Do the evolution

  auto fs = LoopControlQ::loop(
      [=](const iarray<3> &bmin, const iarray<3> &bmax) {
        // TODO: buffer zones are larger
        bool is_sync_boundary = false;
        for (int d = 0; d < 3; ++d)
          is_sync_boundary |= (!ibbox[0][d] && bmin[d] == imin[d]) ||
                              (!ibbox[1][d] && bmax[d] == imax[d]);
        if (is_sync_boundary)
          fs_sync->wait();
      },
      [=](const iarray<3> &ipos) {
        ptrdiff_t ind = ipos[0] * di + ipos[1] * dj + ipos[2] * dk;
        phirhs[ind] =
            -2.0 * (dx2i + dy2i + dz2i) * psi[ind] +
            (dx2i * (psi[ind + di] + psi[ind - di]) +
             dy2i * (psi[ind + dj] + psi[ind - dj]) +
             dz2i * (psi[ind + dk] + psi[ind - dk])) +
            1.0e-12 *
                sin(sin(sin(sin(sin(sin(sin(sin(sin(sin(psi[ind]))))))))));
        psirhs[ind] = phi[ind];
      },
      [](const iarray<3> &bmin, const iarray<3> &bmax) {}, imin, imax, iblock,
      iash);
  move(fs.begin(), fs.end(), back_inserter(fs_rhs));
}

extern "C" void WaveToyMoLFunHPC_FunHPC_Boundaries(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  int ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                                       "wavetoymol::scalarevolve", bound);
  if (ierr)
    CCTK_ERROR("WaveToyMoLFunHPC_FunHPC_Boundaries: Error selecting boundary "
               "condition");
  ps_sync->set_value();
}

extern "C" void WaveToyMoLFunHPC_FunHPC_Finish(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;

  for (auto &f : fs_rhs)
    f.wait();
  fs_rhs.clear();

  assert(ps_sync);
  assert(fs_sync);
  ps_sync.reset();
  fs_sync->wait();
  fs_sync.reset();
}
