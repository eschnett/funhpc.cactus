#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include <qthread/future.hpp>
using namespace qthread;

#include <LoopControlQ.hpp>

#include <algorithm>
#include <array>
#include <cassert>
#include <cstdlib>
#include <iterator>
#include <memory>
#include <vector>
using namespace std;

namespace {
vector<future<void> > fs_rhs;

unique_ptr<promise<void> > ps_sync;
unique_ptr<future<void> > fs_sync;
}

extern "C" void WaveToyMoLFunHPC_FunHPC_Start(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;

  assert(fs_rhs.empty());

  assert(!ps_sync);
  assert(!fs_sync);
  ps_sync = make_unique<promise<void> >();
  fs_sync = make_unique<future<void> >(ps_sync->get_future());
}

extern "C" void WaveToyMoLFunHPC_FunHPC_RHS(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;

  if (*skip_rhs)
    return;

  // Set up shorthands

  CCTK_REAL dx = CCTK_DELTA_SPACE(0);
  CCTK_REAL dy = CCTK_DELTA_SPACE(1);
  CCTK_REAL dz = CCTK_DELTA_SPACE(2);

  CCTK_REAL dx2 = dx * dx;
  CCTK_REAL dy2 = dy * dy;
  CCTK_REAL dz2 = dz * dz;

  CCTK_REAL dx2i = 1.0 / dx2;
  CCTK_REAL dy2i = 1.0 / dy2;
  CCTK_REAL dz2i = 1.0 / dz2;

  int istart = cctk_nghostzones[0];
  int jstart = cctk_nghostzones[1];
  int kstart = cctk_nghostzones[2];

  int iend = cctk_lsh[0] - cctk_nghostzones[0];
  int jend = cctk_lsh[1] - cctk_nghostzones[1];
  int kend = cctk_lsh[2] - cctk_nghostzones[2];

  constexpr ptrdiff_t di = 1;
  ptrdiff_t dj = di * cctk_ash[0];
  ptrdiff_t dk = dj * cctk_ash[1];

  // (16+2*1)^3 * 3*8 B = 140 kB < 256 kB (L2 cache)
  int iblock = 16;
  int jblock = 16;
  int kblock = 16;

  assert(iblock >= cctk_nghostzones[0]);
  assert(jblock >= cctk_nghostzones[1]);
  assert(kblock >= cctk_nghostzones[2]);

  // Do the evolution

  for (int k0 = kstart; k0 < kend; k0 += kblock) {
    for (int j0 = jstart; j0 < jend; j0 += jblock) {
      for (int i0 = istart; i0 < iend; i0 += iblock) {

        int i1 = min(iend, i0 + iblock);
        int j1 = min(jend, j0 + jblock);
        int k1 = min(kend, k0 + kblock);

        // TODO: buffer zones are larger
        bool is_sync_boundary =
            (!cctk_bbox[0] && i0 == istart) || (!cctk_bbox[1] && i1 == iend) ||
            (!cctk_bbox[2] && j0 == jstart) || (!cctk_bbox[3] && j1 == jend) ||
            (!cctk_bbox[4] && k0 == kstart) || (!cctk_bbox[5] && k1 == kend);

        fs_rhs.push_back(async(launch::async, [=]() {
          if (is_sync_boundary)
            fs_sync->wait();
          for (int k = k0; k < k1; k++) {
            for (int j = j0; j < j1; j++) {
              for (int i = i0; i < i1; i++) {
                int ind = i * di + j * dj + k * dk;

                phirhs[ind] =
                    -2.0 * (dx2i + dy2i + dz2i) * psi[ind] +
                    (dx2i * (psi[ind + di] + psi[ind - di]) +
                     dy2i * (psi[ind + dj] + psi[ind - dj]) +
                     dz2i * (psi[ind + dk] + psi[ind - dk])) +
                    1.0e-12 * sin(sin(sin(sin(
                                  sin(sin(sin(sin(sin(sin(psi[ind]))))))))));
                psirhs[ind] = phi[ind];
              }
            }
          }
        }));
      }
    }
  }
}

extern "C" void WaveToyMoLFunHPC_FunHPC_Boundaries(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  int ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                                       "wavetoymol::scalarevolve", bound);
  if (ierr)
    CCTK_ERROR("WaveToyMoLFunHPC_FunHPC_Boundaries: Error selecting boundary "
               "condition");
  ps_sync->set_value();
}

extern "C" void WaveToyMoLFunHPC_FunHPC_Finish(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;

  for (auto &f : fs_rhs)
    f.wait();
  fs_rhs.clear();

  assert(ps_sync);
  assert(fs_sync);
  ps_sync.reset();
  fs_sync->wait();
  fs_sync.reset();
}
