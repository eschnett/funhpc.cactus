#include <cctk.h>
#include <cctk_Arguments.h>

#include <cmath>
using namespace std;

extern "C" void WaveToyMoLFunHPC_Evolution_RegisterEvolved(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;

  int indevolved = CCTK_GroupIndex("wavetoymol::scalarevolve");
  if (indevolved < 0)
    CCTK_ERROR("internal error");
  int indrhs = CCTK_GroupIndex("WaveToyMoLFunHPC::scalarevolverhs");
  if (indrhs < 0)
    CCTK_ERROR("internal error");
  int ierr = MoLRegisterEvolvedGroup(indevolved, indrhs);
  if (ierr)
    CCTK_ERROR("internal error");
}

extern "C" void WaveToyMoLFunHPC_FunHPC_MoL_Begin(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  *MoL_active = 1;
}

extern "C" void WaveToyMoLFunHPC_FunHPC_MoL_CheckSkipRHS(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;

  int rl = GetRefinementLevel(cctkGH);
  int maxrls = GetMaxRefinementLevels(cctkGH);
  bool active = *MoL_active;
  int step = *static_cast<const CCTK_INT *>(
      CCTK_VarDataPtr(cctkGH, 0, "MoL::MoL_Intermediate_Step"));
  int rl_iter_step = lrint(pow(2.0, maxrls - rl - 1));
  *skip_rhs = cctk_iteration % (2 * rl_iter_step) == rl_iter_step + 1 &&
              active && step == 0;
}

extern "C" void WaveToyMoLFunHPC_FunHPC_MoL_End(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  *MoL_active = 0;
}
