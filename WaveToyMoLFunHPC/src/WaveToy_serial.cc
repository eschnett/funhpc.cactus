#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

extern "C" void WaveToyMoLFunHPC_serial_Boundaries(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  int ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                                       "wavetoymol::scalarevolve", bound);
  if (ierr)
    CCTK_ERROR(
        "WaveToyMoLFunHPC_Boundaries: Error selecting boundary condition");
}

extern "C" void WaveToyMoLFunHPC_serial_RHS(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;

  if (*skip_rhs)
    return;

  // Set up shorthands

  CCTK_REAL dx = CCTK_DELTA_SPACE(0);
  CCTK_REAL dy = CCTK_DELTA_SPACE(1);
  CCTK_REAL dz = CCTK_DELTA_SPACE(2);

  CCTK_REAL dx2 = dx * dx;
  CCTK_REAL dy2 = dy * dy;
  CCTK_REAL dz2 = dz * dz;

  CCTK_REAL dx2i = 1.0 / dx2;
  CCTK_REAL dy2i = 1.0 / dy2;
  CCTK_REAL dz2i = 1.0 / dz2;

  int istart = cctk_nghostzones[0];
  int jstart = cctk_nghostzones[1];
  int kstart = cctk_nghostzones[2];

  int iend = cctk_lsh[0] - cctk_nghostzones[0];
  int jend = cctk_lsh[1] - cctk_nghostzones[1];
  int kend = cctk_lsh[2] - cctk_nghostzones[2];

  auto val = [cctkGH](auto gridfunc, auto i, auto j, auto k) {
    return gridfunc[CCTK_GFINDEX3D(cctkGH, i, j, k)];
  };

  // Do the evolution

  for (int k = kstart; k < kend; k++) {
    for (int j = jstart; j < jend; j++) {
      for (int i = istart; i < iend; i++) {
        int vindex = CCTK_GFINDEX3D(cctkGH, i, j, k);

        phirhs[vindex] =
            -2.0 * (dx2i + dy2i + dz2i) * val(psi, i, j, k) +
            (dx2i * (val(psi, i + 1, j, k) + val(psi, i - 1, j, k)) +
             dy2i * (val(psi, i, j + 1, k) + val(psi, i, j - 1, k)) +
             dz2i * (val(psi, i, j, k + 1) + val(psi, i, j, k - 1))) +
            1.0e-12 * sin(sin(sin(sin(
                          sin(sin(sin(sin(sin(sin(val(psi, i, j, k)))))))))));
        psirhs[vindex] = val(phi, i, j, k);
      }
    }
  }
}
