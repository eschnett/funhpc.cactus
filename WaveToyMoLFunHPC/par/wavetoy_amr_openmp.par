ActiveThorns = "
        Boundary
        Carpet
        CarpetIOASCII
        CarpetIOBasic
        CarpetIOScalar
        CarpetLib
        CarpetReduce
        CarpetRegrid2
        CartGrid3D
        CoordBase
        IDScalarWaveMoL
        IOUtil
        InitBase
        MoL
        SymBase
        SystemTopology
        Time
        TimerReport
        WaveToyMoLFunHPC
"

### Domain

grid::type = "coordbase"
Carpet::domain_from_coordbase = yes

CoordBase::domainsize = "minmax"
CoordBase::spacing = "numcells"

CoordBase::xmin = -8.0
CoordBase::ymin = -8.0
CoordBase::zmin = -8.0
CoordBase::xmax = +8.0
CoordBase::ymax = +8.0
CoordBase::zmax = +8.0
CoordBase::ncells_x = 300
CoordBase::ncells_y = 300
CoordBase::ncells_z = 300

Carpet::max_refinement_levels = 10
Carpet::use_buffer_zones = yes
Carpet::prolongation_order_space = 1
Carpet::prolongation_order_time = 1

CarpetRegrid2::num_centres = 1
CarpetRegrid2::num_levels_1 = 6
CarpetRegrid2::radius_1[1] = 4.0
CarpetRegrid2::radius_1[2] = 2.0
CarpetRegrid2::radius_1[3] = 1.0
CarpetRegrid2::radius_1[4] = 0.5
CarpetRegrid2::radius_1[5] = 0.25

MoL::init_RHS_zero = no

MoL::ODE_Method = "RK2"
MoL::MoL_Intermediate_Steps = 2
MoL::MoL_Num_Scratch_Levels = 1
Time::dtfac = 0.25

cactus::cctk_itlast = 512

### Physics

InitBase::initial_data_setup_method = "init_single_level"
Carpet::init_each_timelevel = yes

IDScalarWaveMoL::sigma = 2.8
IDScalarWaveMoL::radius = 0

WaveToyMoL::bound = "flat"
WaveToyMoLFunHPC::parallelization = "OpenMP"

### Output

IO::out_dir = $parfile

IOBasic::outInfo_every = 1
IOBasic::outInfo_vars = "wavetoymol::phi"

IOScalar::one_file_per_group = yes
IOScalar::all_reductions_in_one_file = yes
IOScalar::outScalar_every = 512
IOScalar::outScalar_vars = "wavetoymol::phi"

IOASCII::one_file_per_group = yes
IOASCII::compact_format = yes
IOASCII::out1D_every = 512
IOASCII::out1D_vars = "wavetoymol::phi"

TimerReport::out_every = 512
TimerReport::out_filename = "TimerReport"
TimerReport::n_top_timers = 50
