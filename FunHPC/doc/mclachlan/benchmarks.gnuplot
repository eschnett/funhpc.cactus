set terminal postscript color enhanced eps
set output "benchmarks.eps"

set size 0.7
set key top left

set title "McLachlan Unigrid Scaling Benchmark (Stampede)"

set xlabel "cores"
set log x

set ylabel "CPU time per grid point [core seconds]"
set yrange [0:]

ncores=16
niters=10*4   # RK4

plot \
"< grep openmp benchmarks-100.dat" using ($2*ncores):($4*$2*ncores/(niters*(100-1)**3)) title "OpenMP (100^3)" w lp lw 3, \
"< grep funhpc benchmarks-100.dat" using ($2*ncores):($4*$2*ncores/(niters*(100-1)**3)) title "FunHPC (100^3)" w lp lw 3
