set terminal postscript color enhanced eps
set output "benchmarks-amr.eps"

set size 0.7
set key top left

set title "McLachlan AMR Scaling Benchmark (Stampede)"

set xlabel "cores"
set log x

set ylabel "CPU time per grid point [core seconds]"
set yrange [0:]

ncores=16
# 512 iterations, fine grid step is 16, x2 for all coarser grids, x2 for RK integrator
niters=(512/16)*2*4   # RK4

plot \
"< grep openmp benchmarks-amr-100.dat" using ($2*ncores):(($4+$9)*$2*ncores/(niters*(100-1)**3)) title "OpenMP (100^3)" w lp lw 3, \
"< grep funhpc benchmarks-amr-100.dat" using ($2*ncores):(($4+$9)*$2*ncores/(niters*(100-1)**3)) title "FunHPC (100^3)" w lp lw 3
