set terminal postscript color enhanced eps
set output "benchmarks-stampede.eps"

set size 0.7
set key right center

set title "Unigrid Scaling Benchmark, OpenMP and FunHPC (Stampede)"

set xlabel "cores"
set log x

set ylabel "CPU time per grid point [core seconds]"
set yrange [0:]

ncores=16
niters=10*2   # RK2

plot \
"< grep openmp benchmarks-600.dat" using ($2*ncores):($4*$2*ncores/(niters*(600-1)**3)) title "OpenMP (600^3)" w lp lw 3, \
"< grep funhpc benchmarks-600.dat" using ($2*ncores):($4*$2*ncores/(niters*(600-1)**3)) title "FunHPC (600^3)" w lp lw 3
