set terminal postscript color enhanced eps
set output "benchmarks-amr.eps"

set size 0.7
set key right center

set title "AMR Scaling Benchmark, OpenMP and FunHPC (Zwicky)"

set xlabel "cores"
set log x

set ylabel "CPU time per grid point [core seconds]"
set yrange [0:]

ncores=12
# 512 iterations, fine grid step is 16, x2 for all coarser grids, x2 for RK integrator
niters=(512/16)*2*2   # RK2

plot \
"< grep openmp benchmarks-amr-300.dat" using ($2*ncores):(($4+$9)*$2*ncores/(niters*(300-1)**3)) title "OpenMP (300^3)" w lp lw 3, \
"< grep openmp benchmarks-amr-600.dat" using ($2*ncores):(($4+$9)*$2*ncores/(niters*(600-1)**3)) title "OpenMP (600^3)" w lp lw 3, \
"< grep funhpc benchmarks-amr-300.dat" using ($2*ncores):(($4+$9)*$2*ncores/(niters*(300-1)**3)) title "FunHPC (300^3)" w lp lw 3, \
"< grep funhpc benchmarks-amr-600.dat" using ($2*ncores):(($4+$9)*$2*ncores/(niters*(600-1)**3)) title "FunHPC (600^3)" w lp lw 3
