#! /bin/bash

################################################################################
# Configure Cactus
################################################################################

# Pass options to Cactus
echo "BEGIN MAKE_DEFINITION"
echo "FUNHPC_DIR = ${FUNHPC_DIR}"
echo "FUNHPC_INC_DIRS = ${FUNHPC_DIR}/include ${CEREAL_DIR}/include ${JEMALLOC_DIR}/include ${QTHREADS_DIR}/include"
echo "FUNHPC_LIB_DIRS = ${FUNHPC_DIR}/lib ${CEREAL_DIR}/lib ${JEMALLOC_DIR}/lib ${QTHREADS_DIR}/lib"
echo "FUNHPC_LIBS = funhpc qthread jemalloc"
echo "END MAKE_DEFINITION"

# Pass configuration options to other thorns
echo 'INCLUDE_DIRECTORY $(FUNHPC_INC_DIRS)'
echo 'LIBRARY_DIRECTORY $(FUNHPC_LIB_DIRS)'
echo 'LIBRARY $(FUNHPC_LIBS)'
