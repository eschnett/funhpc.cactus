#include "LoopControlQ.hpp"

#include <cctk.h>
#include <cctk_Parameters.h>

namespace LoopControlQ {

bool get_veryverbose() {
  DECLARE_CCTK_PARAMETERS;
  return veryverbose;
}
}
