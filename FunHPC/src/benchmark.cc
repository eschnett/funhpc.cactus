#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include <qthread/future.hpp>
#include <qthread/thread.hpp>

#include <omp.h>

#include <cassert>
#include <functional>
#include <iostream>
#include <vector>

namespace FunHPC {
using namespace qthread;
using namespace std;

void runbench(const string &name, int mintasks, int maxtasks, double mintime,
              const function<int(int)> &kernel, int &ntasks, double &time) {
  DECLARE_CCTK_PARAMETERS;
  assert(mintasks > 0);
  CCTK_Barrier(nullptr);
  ntasks = mintasks;
  for (;;) {
    if (verbose)
      cout << name << ": ntasks=" << ntasks << "...\n" << flush;
    auto t0 = omp_get_wtime();
    int r = kernel(ntasks);
    auto t1 = omp_get_wtime();
    if (r != ntasks) {
      cerr << name << ": ntasks=" << ntasks << " r=" << r << "\n" << flush;
    }
    assert(r == ntasks);
    time = t1 - t0;
    if (time >= mintime || ntasks == maxtasks)
      break;
    ntasks = min(maxtasks, ntasks * 2);
  }
  CCTK_Barrier(nullptr);
}

namespace {

int worker() { return 1; }

int serial(int count) {
  if (count <= 0)
    return 0;
  return async(launch::async, serial, count - 1).get() + worker();
}

int parallel(int count) {
  vector<future<int> > fs;
  fs.reserve(count);
  for (int i = 0; i < count; ++i)
    fs.push_back(async(launch::async, worker));
  int r = 0;
  for (auto &f : fs)
    r += f.get();
  return r;
}

int tree(int count) {
  if (count <= 0)
    return 0;
  if (count == 1)
    return async(launch::async, worker).get();
  count -= 2;
  int count1 = count / 2;
  int count2 = count - count1;
  assert(count1 >= 0 && count1 <= count && (count <= 1 || count1 != 0));
  assert(count2 >= 0 && count2 <= count && (count <= 1 || count2 != 0));
  auto f1 = async(launch::async, tree, count1);
  auto f2 = async(launch::async, tree, count2);
  return f1.get() + worker() + f2.get() + worker();
}
}

extern "C" void FunHPC_Benchmark(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  CCTK_INFO("FunHPC multi-threading:");

  const int nthreads = thread::hardware_concurrency();
  cout << "There are " << nthreads << " hardware threads available\n";

  const int mintasks = 10;
  const int maxtasks = 1000000000;
  const double mintime = 0.1;
  int ntasks;
  double time;

  runbench("serial", mintasks, maxtasks, mintime, serial, ntasks, time);
  cout << "Started " << ntasks << " serial tasks, elapsed " << time << " sec; "
       << (1.0e+9 * time / ntasks) << " nsec/task\n";

  runbench("parallel", mintasks, maxtasks, mintime, parallel, ntasks, time);
  cout << "Started " << ntasks << " parallel tasks, elapsed " << time
       << " sec; " << (1.0e+9 * time / ntasks) << " nsec/task\n";

  runbench("tree", mintasks, maxtasks, mintime, tree, ntasks, time);
  cout << "Started " << ntasks << " tree tasks, elapsed " << time << " sec; "
       << (1.0e+9 * time / ntasks) << " nsec/task\n";
}
}
