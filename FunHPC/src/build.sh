#! /bin/bash

################################################################################
# Build
################################################################################

# Set up shell
if [ "$(echo ${VERBOSE} | tr '[:upper:]' '[:lower:]')" = 'yes' ]; then
    set -x                      # Output commands
fi
set -e                          # Abort on errors



# Set locations
THORN=FunHPC
NAME=funhpc
SRCDIR="$(dirname $0)"
BUILD_DIR=${SCRATCH_BUILD}/build/${THORN}
if [ -z "${FUNHPC_INSTALL_DIR}" ]; then
    INSTALL_DIR=${SCRATCH_BUILD}/external/${THORN}
else
    echo "BEGIN MESSAGE"
    echo "Installing FunHPC into ${FUNHPC_INSTALL_DIR}"
    echo "END MESSAGE"
    INSTALL_DIR=${FUNHPC_INSTALL_DIR}
fi
DONE_FILE=${SCRATCH_BUILD}/done/${THORN}
FUNHPC_DIR=${INSTALL_DIR}

# Set up environment
export LIBS='-lm'
unset RPATH
if echo '' ${ARFLAGS} | grep 64 >/dev/null 2>&1; then
    export OBJECT_MODE=64
fi

echo "FunHPC: Preparing directory structure..."
cd ${SCRATCH_BUILD}
mkdir build external done 2> /dev/null || true
rm -rf ${BUILD_DIR} ${INSTALL_DIR}
mkdir ${BUILD_DIR} ${INSTALL_DIR}

echo "FunHPC: Cloning repository..."
pushd ${BUILD_DIR}
${GIT} clone https://bitbucket.org/eschnett/funhpc.cxx

echo "FunHPC: Configuring..."
./configure --prefix=${FUNHPC_DIR}

echo "FunHPC: Building..."
cd ${NAME}
${MAKE}                                                                       \
    SIMFACTORY_GCC_DIR=1                                                      \
    CXX="${CXX}"                                                              \
    MPICXX=mpicxx                                                             \
    MPIRUN=mpirun                                                             \
    SIMFACTORY_CEREAL_DIR="${CEREAL_DIR}"                                     \
    SIMFACTORY_CEREAL_CXXFLAGS=$(echo '' $(for dir in ${CEREAL_INC_DIRS};     \
                                           do echo " -I$dir";                 \
                                           done))                             \
    SIMFACTORY_CEREAL_LDFLAGS=$(echo '' $(for dir in ${CEREAL_LIB_DIRS};      \
                                          do echo " -L$dir";                  \
                                          done))                              \
    SIMFACTORY_CEREAL_LIBS=$(echo '' $(for lib in ${CEREAL_LIBS};             \
                                       do echo " -l$lib";                     \
                                       done))                                 \
    SIMFACTORY_HWLOC_DIR="${HWLOC_DIR}"                                       \
    SIMFACTORY_HWLOC_CXXFLAGS=$(echo '' $(for dir in ${HWLOC_INC_DIRS};       \
                                          do echo " -I$dir";                  \
                                          done))                              \
    SIMFACTORY_HWLOC_LDFLAGS=$(echo '' $(for dir in ${HWLOC_LIB_DIRS};        \
                                         do echo " -L$dir";                   \
                                         done))                               \
    SIMFACTORY_HWLOC_LIBS=$(echo '' $(for lib in ${HWLOC_LIBS};               \
                                      do echo " -l$lib";                      \
                                      done))                                  \
    SIMFACTORY_JEMALLOC_DIR="${JEMALLOC_DIR}"                                 \
    SIMFACTORY_JEMALLOC_CXXFLAGS=$(echo '' $(for dir in ${JEMALLOC_INC_DIRS}; \
                                             do echo " -I$dir";               \
                                             done))                           \
    SIMFACTORY_JEMALLOC_LDFLAGS=$(echo '' $(for dir in ${JEMALLOC_LIB_DIRS};  \
                                            do echo " -L$dir";                \
                                            done))                            \
    SIMFACTORY_JEMALLOC_LIBS=$(echo '' $(for lib in ${JEMALLOC_LIBS};         \
                                         do echo " -l$lib";                   \
                                         done))                               \
    SIMFACTORY_QTHREADS_DIR="${QTHREADS_DIR}"                                 \
    SIMFACTORY_QTHREADS_CXXFLAGS=$(echo '' $(for dir in ${QTHREADS_INC_DIRS}; \
                                             do echo " -I$dir";               \
                                             done))                           \
    SIMFACTORY_QTHREADS_LDFLAGS=$(echo '' $(for dir in ${QTHREADS_LIB_DIRS};  \
                                            do echo " -L$dir";                \
                                            done))                            \
    SIMFACTORY_QTHREADS_LIBS=$(echo '' $(for lib in ${QTHREADS_LIBS};         \
                                         do echo " -l$lib";                   \
                                         done))                               \
    lib                                                                       \
    selftest selftest-funhpc                                                  \
    benchmark benchmark2 fibonacci hello pingpong

echo "FunHPC: Installing..."
rm -rf ${FUNHPC_DIR}/bin
rm -rf ${FUNHPC_DIR}/include
rm -rf ${FUNHPC_DIR}/lib

mkdir ${FUNHPC_DIR}/bin
for binfile in selftest selftest-funhpc benchmark benchmark2 fibonacci hello pingpong; do
    cp -p ${binfild} ${FUNHPC_DIR}/bin
done

mkdir ${FUNHPC_DIR}/include
for subdir in adt cxx fun funhpc qthread; do
    mkdir -p ${FUNHPC_DIR}/include/${subdir}
    for incfile in subdir/*.hpp; do
        cp -p ${incfile} ${FUNHPC_DIR}/include/${subdir}
    done
done
mkdir ${FUNHPC_DIR}/lib
for libfile in libfunhpc.a; do
    cp -p ${libfile} ${FUNHPC_DIR}/lib
done

popd

echo "FunHPC: Cleaning up..."
rm -rf ${BUILD_DIR}

date > ${DONE_FILE}
echo "FunHPC: Done."
