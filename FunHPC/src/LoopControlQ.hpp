#ifndef LOOPCONTROLQ_HPP
#define LOOPCONTROLQ_HPP

#include <cctk.h>
#include <cctk_Parameters.h>

#include <cxx/cstdlib.hpp>
#include <qthread/future.hpp>

#include <algorithm>
#include <array>
#include <cassert>
#include <cstdlib>
#include <type_traits>
#include <utility>
#include <vector>

namespace LoopControlQ {
using namespace cxx;
using namespace qthread;
using namespace std;

/******************************************************************************/
/* Cactus Parameters */

extern bool get_veryverbose();

/******************************************************************************/
/* Integer arrays */

template <size_t D> using iarray = array<ptrdiff_t, D>;

/******************************************************************************/
/* Evaluate an if condition at compile time */

template <bool C, typename F, typename G>
inline enable_if_t<C, void> cond(F &&f, G &&g) {
  forward<F>(f)();
}

template <bool C, typename F, typename G>
inline enable_if_t<!C, void> cond(F &&f, G &&g) {
  forward<G>(g)();
}

/******************************************************************************/
/* Multi-dimensional loops, with or without explicitly given step size */

// With stride as compile-time argument

namespace detail {
template <ptrdiff_t istr0, bool aligned_interior, size_t D, typename F,
          size_t D1>
inline enable_if_t<(D == 0), void>
nloops_str(F &&f, const iarray<D1> &imin, const iarray<D1> &imax,
           const iarray<D1> &iash, const ptrdiff_t ialn0, const ptrdiff_t ioff0,
           const iarray<D1> &ipos) {
  static_assert(D <= D1, "");
  forward<F>(f)(imin, imax, ipos);
}

template <ptrdiff_t istr0, bool aligned_interior, size_t D, typename F,
          size_t D1>
inline enable_if_t<(D == 1), void>
nloops_str(const F &f, const iarray<D1> &imin, const iarray<D1> &imax,
           const iarray<D1> &iash, const ptrdiff_t ialn0, const ptrdiff_t ioff0,
           iarray<D1> ipos) {
  static_assert(D <= D1, "");
  ptrdiff_t iloc = 0;
  if (!aligned_interior) {
    for (size_t d = D1 - 1; d >= 1; --d)
      iloc = (iloc + ipos[d]) * iash[d - 1];
  }
  iloc += imin[0];
  iloc += ioff0;
  ptrdiff_t imod0 = iloc - align_floor(iloc, istr0, 0);
  for (ptrdiff_t i = imin[0] - imod0; i < imax[0]; i += istr0) {
    ipos[0] = i;
    nloops_str<istr0, aligned_interior, 0>(f, imin, imax, iash, ialn0, ioff0,
                                           ipos);
  }
}

template <ptrdiff_t istr0, bool aligned_interior, size_t D, typename F,
          size_t D1>
inline enable_if_t<(D > 1), void>
nloops_str(const F &f, const iarray<D1> &imin, const iarray<D1> &imax,
           const iarray<D1> &iash, const ptrdiff_t ialn0, const ptrdiff_t ioff0,
           iarray<D1> ipos) {
  static_assert(D <= D1, "");
  for (ptrdiff_t i = imin[D - 1]; i < imax[D - 1]; ++i) {
    ipos[D - 1] = i;
    nloops_str<istr0, aligned_interior, D - 1>(f, imin, imax, iash, ialn0,
                                               ioff0, ipos);
  }
}
}

template <ptrdiff_t istr0, bool aligned_interior, size_t D, typename F>
inline void nloops_str(F &&f, const iarray<D> &imin, const iarray<D> &imax,
                       const iarray<D> &iash, const ptrdiff_t ialn0,
                       const ptrdiff_t ioff0) {
  {
    const iarray<D> ipos __attribute__((__unused__)){};
    static_assert(
        is_same<decltype(forward<F>(f)(imin, imax, ipos)), void>::value, "");
  }
  iarray<D> ipos;
  detail::nloops_str<istr0, aligned_interior, D>(forward<F>(f), imin, imax,
                                                 iash, ialn0, ioff0, ipos);
}

// With step size as run-time argument

namespace detail {
template <size_t D, typename F, size_t D1>
inline enable_if_t<(D == 0), void>
nloops(F &&f, const iarray<D1> &imin, const iarray<D1> &imax,
       const iarray<D1> &istep, const iarray<D1> &ipos) {
  static_assert(D <= D1, "");
  forward<F>(f)(ipos);
}

template <size_t D, typename F, size_t D1>
inline enable_if_t<(D > 0), void>
nloops(const F &f, const iarray<D1> &imin, const iarray<D1> &imax,
       const iarray<D1> &istep, iarray<D1> ipos) {
  static_assert(D <= D1, "");
  for (ptrdiff_t i = imin[D - 1]; i < imax[D - 1]; i += istep[D - 1]) {
    ipos[D - 1] = i;
    nloops<D - 1>(f, imin, imax, istep, ipos);
  }
}
}

template <size_t D, typename F>
inline void nloops(F &&f, const iarray<D> &imin, const iarray<D> &imax,
                   const iarray<D> &istep) {
  {
    const iarray<D> ipos __attribute__((__unused__)){};
    static_assert(is_same<decltype(forward<F>(f)(ipos)), void>::value, "");
  }
  iarray<D> ipos;
  detail::nloops<D>(forward<F>(f), imin, imax, istep, ipos);
}

// Without stride or step size

template <size_t D, typename F>
inline void nloops(F &&f, const iarray<D> &imin, const iarray<D> &imax) {
  nloops_str<1, false, D>(
      [=](const iarray<D> &imin, const iarray<D> &imax, const iarray<D> &ipos) {
        static_assert(is_same<decltype(f(ipos)), void>::value, "");
        f(ipos);
      },
      imin, imax);
}

/******************************************************************************/
/* Blocked, multi-threaded, multi-dimensional loop */

// With stride

template <ptrdiff_t istr0, bool aligned_interior, typename Init, typename F,
          typename Fini, size_t D>
vector<future<void> > loop_str(const Init &init, const F &f, const Fini &fini,
                               const iarray<D> &imin, const iarray<D> &imax,
                               const iarray<D> &iblock, const iarray<D> &iash,
                               const ptrdiff_t ialn0, const ptrdiff_t ioff0) {
  const bool veryverbose = get_veryverbose();

  vector<future<void> > fs;

  static_assert(istr0 > 0, "");
  for (size_t d = 0; d < D; ++d)
    assert(iblock[d] > 0);
  assert(ioff0 >= 0 && ioff0 < ialn0);
  assert(iblock[0] % ialn0 == 0);
  // assert(iblock[0] % istr0 == 0);

  iarray<D> jmin, jmax, jstep;
  for (size_t d = 0; d < D; ++d) {
    jstep[d] = iblock[d];
    jmin[d] = align_floor(imin[d], jstep[d], d == 0 ? ioff0 : 0);
    assert(jmin[d] <= imin[d]);
    assert(jmin[d] + jstep[d] > imin[d]);
    jmax[d] = imax[d];
  }

  if (veryverbose) {
    CCTK_VINFO("Loop [%td,%td,%td]:[%td,%td,%td] blocked [%td,%td,%td]",
               imin[0], imin[1], imin[2], imax[0], imax[1], imax[2], iblock[0],
               iblock[1], iblock[2]);
    CCTK_VINFO("Layout ash [%td,%td,%td] aln %td off %td str %td", iash[0],
               iash[1], iash[2], ialn0, ioff0, istr0);
    CCTK_VINFO("Tiles [%td,%td,%td]:[%td,%td,%td] blocked [%td,%td,%td]",
               jmin[0], jmin[1], jmin[2], jmax[0], jmax[1], jmax[2], jstep[0],
               jstep[1], jstep[2]);
  }

  nloops(
      [&](const iarray<D> &jpos) {

        iarray<D> kmin, kmax;
        for (size_t d = 0; d < D; ++d) {
          kmin[d] = max(imin[d], jpos[d]);
          kmax[d] = min(imax[d], jpos[d] + jstep[d]);
        }

        if (veryverbose)
          CCTK_VINFO("Tile [%td,%td,%td]:[%td,%td,%td]", kmin[0], kmin[1],
                     kmin[2], kmax[0], kmax[1], kmax[2]);

        fs.push_back(async(launch::async, [=]() {

          init(kmin, kmax);
          nloops_str<istr0, aligned_interior>(f, kmin, kmax, iash, ialn0,
                                              ioff0);
          fini(kmin, kmax);

        }));

      },
      jmin, jmax, jstep);

  return fs;
}

template <ptrdiff_t istr0, bool aligned_interior, typename F, size_t D>
vector<future<void> > loop_str(const F &f, const iarray<D> &imin,
                               const iarray<D> &imax, const iarray<D> &iblock,
                               const iarray<D> &iash, const ptrdiff_t ialn0,
                               const ptrdiff_t ioff0) {
  {
    const iarray<D> ipos __attribute__((__unused__)){};
    static_assert(is_same<decltype(f(imin, imax, ipos)), void>::value, "");
  }
  return loop_str<istr0, aligned_interior>(
      [](const iarray<D> &jmin, const iarray<D> &jmax) {}, f,
      [](const iarray<D> &jmin, const iarray<D> &jmax) {}, imin, imax, iblock,
      iash, ialn0, ioff0);
}

// Without stride

template <typename Init, typename F, typename Fini, size_t D>
vector<future<void> > loop(const Init &init, const F &f, const Fini &fini,
                           const iarray<D> &imin, const iarray<D> &imax,
                           const iarray<D> &iblock, const iarray<D> &iash) {
  {
    const iarray<D> ipos __attribute__((__unused__)){};
    static_assert(is_same<decltype(f(ipos)), void>::value, "");
  }
  return loop_str<1, false>(init,
                            [=](const iarray<D> &imin, const iarray<D> &imax,
                                const iarray<D> &ipos) { f(ipos); },
                            fini, imin, imax, iblock, iash, 1, 0);
}

template <typename F, size_t D>
vector<future<void> > loop(const F &f, const iarray<D> &imin,
                           const iarray<D> &imax, const iarray<D> &iblock,
                           const iarray<D> &iash) {
  {
    const iarray<D> ipos __attribute__((__unused__)){};
    static_assert(is_same<decltype(f(ipos)), void>::value, "");
  }
  return loop_str<1>([=](const iarray<D> &imin, const iarray<D> &imax,
                         const iarray<D> &ipos) { f(ipos); },
                     imin, imax, iblock, iash, 1, 0);
}
}

#endif // #ifndef LOOPCONTROLQ_HPP
