#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include <cmath>
using namespace std;

extern "C" void IDScalarWaveMoL_InitialData(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

#pragma omp parallel for collapse(3)
  for (int k = 0; k < cctk_lsh[2]; k++) {
    for (int j = 0; j < cctk_lsh[1]; j++) {
      for (int i = 0; i < cctk_lsh[0]; i++) {
        int vindex = CCTK_GFINDEX3D(cctkGH, i, j, k);

        CCTK_REAL R = r[vindex];

        phi[vindex] = amplitude * exp(-pow((R - radius) / sigma, 2));
        psi[vindex] = 0.0;
      }
    }
  }
}
